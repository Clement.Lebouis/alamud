import  networkx
import  matplotlib.pyplot  as  pyplot
pyplot.ion ()
pyplot.show ()
G=networkx.Graph ()
G.add_edge ('arbre' ,'parking-iut')
G.add_edge ('arbre' ,'nid')
G.add_edge ('arbre' ,'badge')
G.add_edge ('parking-iut' ,'porche-iut')
G.add_edge ('porche-iut' ,'hall-iut')
G.add_edge ('hall-iut' ,'etage1-iut')
G.add_edge ('etage1-iut' ,'i22')
G.add_edge ('etage1-iut' ,'i23')
G.add_edge ('Licorne' ,'i23')
G.add_edge ('Briquet' ,'i23')
G.add_edge ('i22' ,'cabane')
G.add_edge ('flambeau' ,'cabane')
G.add_edge ('hall-iut' ,'sousSol')
G.add_edge ('Enfer' ,'sousSol')
G.add_edge ('Enfer' ,'SalleOr')
G.add_edge ('Raptor jesus' ,'SalleOr')
G.add_edge ('Enfer' ,'SalleArgent')
G.add_edge ('Van helsing' ,'SalleArgent')
G.add_edge ('Amiral snack Bar','SalleChair')
G.add_edge ('Enfer' ,'SalleBronze')
G.add_edge ('leeroy jenkins' ,'SalleBronze')
G.add_edge ('Enfer' ,'SalleChair')
G.add_edge ('Enfer' ,'Couloir de la Perdition')
G.add_edge ('Couloir de la Perdition' , 'salle 1')
G.add_edge ('Couloir de la Perdition' , 'salle 2')
G.add_edge ('Couloir de la Perdition' , 'salle 3')
G.add_edge ('salle 4' , 'salle 3')
G.add_edge ('salle 2' , 'salle 5')
G.add_edge ('salle 1' , 'salle 6')
G.add_edge ('salle 1' , 'salle 13')
G.add_edge ('salle 5' , 'salle 3')
G.add_edge ('salle 2' , 'salle 7')
G.add_edge ('salle 7' , 'salle 8')
G.add_edge ('salle 10' , 'salle 11')
G.add_edge ('salle 12' , 'salle 8')
G.add_edge ('salle 7' , 'salle 11')
G.add_edge ('salle 7' , 'salle 13')
G.add_edge ('salle 4' , 'salle clé')
G.add_edge ('pot' , 'salle clé')
G.add_edge ('pot' , 'clé')
G.add_edge ('salle 10' , 'salle clé')
G.add_edge ('salle 10' , 'salle piégé')
G.add_edge ('salle 7' , 'salle piégé')
G.add_edge ('salle 4' , 'salle piégé')
G.add_edge ('miroir' , 'salle piégé')
G.add_edge ('salle 9' , 'salle Final')
G.add_edge ('salle 9' , 'salle piégé2')
G.add_edge ('dalle' , 'salle piégé2')
G.add_edge ('salle 9' , 'salle hasard')
G.add_edge ('salle 13' , 'salle hasard')
G.add_edge ('cartes' , 'table')
G.add_edge ('table' , 'salle hasard')
G.add_edge ('sousSol' , 'salle Final')
G.add_edge ('pieces' , 'salle Final')
G.add_edge ('pancarte' , 'salle Final')
G.add_node ('dev')





dicopos={
'arbre': (0,1),
'nid': (0,1.5),
'badge' : (0,2),
'parking-iut':(0,0),
'porche-iut' :(1,0),
'hall-iut':(2,0),
'etage1-iut' :(3,0),
'i22':(4,0),
'cabane':(4,-1),
'flambeau' : (4.5,-1),
'i23' :(3,1),
'Briquet' : (2.5,1.5),
'Licorne' : (3.5,1.5),
'sousSol' :(2,-1),
'Enfer' : (2,-2),
'SalleOr':(1.5,-1.5),
'Raptor jesus' : (1,-1.5),
'SalleArgent' : (2.5,-1.5),
'Van helsing' : (3,-1.5),
'SalleBronze' : (1.5 , -2.5),
'leeroy jenkins': (1,-2.5),
'SalleChair' : (2.5, -2.5),
'Amiral snack Bar': (3,-2.5),
'Couloir de la Perdition':(2,-3),
'salle 1' : (3, -3),
'salle 2' : (2, -4),
'salle 3' : (1, -3),
'salle 4' : (0, -3),
'salle 5' : (1, -4),
'salle 6' : (4, -3),
'salle 7' : (2, -5),
'salle 8' : (2, -6),
'salle 9' : (4, -6),
'salle 10': (0, -5),
'salle 11': (0, -6),
'salle 12': (1, -6),
'salle 13': (3, -5),
'salle Final': (6, -2),
'pieces' : (6,-2.5),
'pancarte' : (6,-1.5),
'salle clé' : (0, -4),
'pot' : (-0.5, -4),
'clé' : (-1, -4),
'miroir' :(1.5,-4.5),
'dalle' : (3,-6.5),
'cartes' : (4,-4),
'table' : (4,-4.5),
'salle piégé' : (1, -5),
'salle piégé2' : (3, -6),
'salle hasard' : (4, -5),
'dev' : (5,3)
}

dicoportail={
('arbre' ,'parking-iut'): "portal-parking-arbre",
('parking-iut' ,'porche-iut'):"portal-parking-porche",
('porche-iut' ,'hall-iut'):"portal-porche-hall",
('hall-iut' ,'etage1-iut'):"escalier-etage",
('etage1-iut' ,'i22'):"etage-salle-22",
('etage1-iut' ,'i23'):"etage-salle-23",
('i22' ,'cabane'):"ordinateur-cabane",
('hall-iut' ,'sousSol'):"escalier-666",
('Enfer' ,'sousSol'):"trou",
('Enfer' ,'SalleOr'):"enfer-Or",
('Enfer' ,'SalleArgent'):"enfer-Argent",
('Enfer' ,'SalleBronze'):"enfer-Bronze",
('Enfer' ,'SalleChair'):"enfer-Chair",
('Enfer' ,'Couloir de la Perdition'):"enfer-couloir",
('Couloir de la Perdition' , 'salle 1'):"couloir-s1",
('Couloir de la Perdition' , 'salle 2'):"couloir-s2",
('Couloir de la Perdition' , 'salle 3'):"couloir-s3",
('salle 4' , 'salle 3'):"s4-s3",
('salle 2' , 'salle 5'):"s2-s5",
('salle 1' , 'salle 6'):"s1-s6",
('salle 1' , 'salle 13'):"s1-s13",
('salle 5' , 'salle 3'):"s3-s5",
('salle 2' , 'salle 7'):"s7-s2",
('salle 7' , 'salle 8'):"s7-s8",
('salle 10' , 'salle 11'):"s10-s11",
('salle 12' , 'salle 8'):"s12-s8",
('salle 7' , 'salle 11'):"s7-s11",
('salle 7' , 'salle 13'):"s7-s13",
('salle 4' , 'salle clé'):"s4-scle",
('salle 10' , 'salle clé'):"s10-scle",
('salle 10' , 'salle piégé'):"s10-piégé",
('salle 7' , 'salle piégé'):"s7-piégé",
('salle 4' , 'salle piégé'):"s4-piégé",
('salle 9' , 'salle Final'):"s9-final",
('salle 9' , 'salle piégé2'):"s9-piégé2",
('salle 9' , 'salle hasard'):"s9-hasard",
('salle 13' , 'salle hasard'):"s13-hasard",
('sousSol' , 'salle Final'):"final-soussol",
}



'''
En rouge se sont les salles
en vert se sont les objets 
en bleu se sont les containers
'''
objet=['Raptor jesus','Van helsing','pieces','pancarte','Briquet','Licorne',"clé","Amiral snack Bar",'flambeau','leeroy jenkins','miroir','dalle','cartes','badge']
container =['pot','nid','table']
#code couleur : rouge = salle, bleu = container, vert = thing
networkx.draw (G, with_labels=True ,pos=dicopos)
networkx.draw_networkx_edge_labels(G,dicopos,edge_labels=dicoportail)
networkx.draw_networkx_nodes(G,dicopos,edge_labels=dicoportail, nodelist=container, node_color='blue',with_labels=True)
networkx.draw_networkx_nodes(G,dicopos,edge_labels=dicoportail, nodelist=objet, node_color='green',with_labels=True)