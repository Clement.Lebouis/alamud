# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TalkEvent(Event2):
    NAME = "talk"

    def perform(self):
        if not self.object.has_prop("talkable"):
            self.add_prop("object-not-talkable")
            return self.take_failed()
        self.inform("talk")

    def take_failed(self):
        self.fail()
        self.inform("talk.failed")

